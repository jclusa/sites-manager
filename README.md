# Sites Manager App

React CRUD App

### [Demo](https://sites-manager.netlify.app/)

This codebase was created to demonstrate a CRUD frontend application built with React, Typescript, and Redux Toolkit including CRUD operations, routing and more.

## Features

- [x] Manage data loading feedback and error management
- [x] Responsive app
- [x] Clean and Simple Material UI
- [x] Redux State Management
- [x] Tests with high coverage threshold

# How it works

The root of the application is the `src/components/App` component. The App component uses react-router's HashRouter to display the different pages. Each page is represented by a [function component](https://reactjs.org/docs/components-and-props.html).

Some components include a `.slice` file that contains the definition of its state and reducers, which might also be used by other components. These slice files follow the [Redux Toolkit](https://redux-toolkit.js.org/) guidelines. Components connect to the state by using [custom hooks](https://reactjs.org/docs/hooks-custom.html#using-a-custom-hook).

This application is built following (as much as practicable) functional programming principles:

- Immutable Data
- No classes
- No let or var
- Use of monads (Option, Result)
- No side effects

The code avoids runtime type-related errors by using Typescript and decoders for data coming from the API.

Some components include a `.test` file that contains unit tests.

This project uses prettier and eslint to enforce a consistent code syntax.

## Folder structure

- `src/components` Contains all the functional components.
- `src/components/Pages` Contains the components used by the router as pages.
- `src/config` Contains configuration files.
- `src/services` Contains the code that interacts with external systems (API requests).
- `src/state` Contains redux related code.
- `src/types` Contains type definitions alongside the code related to those types.
- `src/utils` Contains utility functions.

## Built with

- **@hqoss/monads**: for passing to the UI when data is loading or there has been an error using a single variable.
- **@mui/material**: for importing some simple React components.
- **@reduxjs/toolkit**: for Redux State Management.
- **axios**: provides an easy-to-use API in a compact package for most of your HTTP communication needs.
- **decoders**: Elegant and battle-tested validation library for type-safe input data for TypeScript
- **react-router-dom**: add routing to react apps
- **typescript**: strongly typed programming language that builds on JavaScript

# Getting started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
