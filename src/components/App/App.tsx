import { HashRouter, Route, Routes } from 'react-router-dom';
import { Home } from '../Pages/Home/Home';
import { ShowSite } from '../Pages/ShowSite/ShowSite';
import { CreateSite } from '../Pages/CreateSite/CreateSite';
import { EditSite } from '../Pages/EditSite/EditSite';
import { Fragment } from 'react';
import { Header } from '../Header/Header';

export function App() {
  return (
    <HashRouter>
      <Fragment>
        <Header />
        <Routes>
          <Route path="/sites/create" element={<CreateSite />} />
          <Route path="/sites/:_id" element={<ShowSite />} />
          <Route path="/sites/:_id/edit" element={<EditSite />} />
          <Route path="/" element={<Home />} />
        </Routes>
      </Fragment>
    </HashRouter>
  );
}
