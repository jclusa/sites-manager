import { GenericError } from '../../types/error';
import Typography from '@mui/material/Typography';

export function Error({ error }: { error: GenericError }) {
  return (
    <div>
      {error.error && (
        <Typography variant="overline" gutterBottom color={'red'}>
          {error.error}
        </Typography>
      )}
    </div>
  );
}
