import TextField from '@mui/material/TextField';

export function FormGroup({
  type,
  label,
  disabled,
  value,
  onChange,
}: {
  type: string;
  label: string;
  disabled: boolean;
  value: string;
  onChange: (ev: React.ChangeEvent<HTMLInputElement>) => void;
}) {
  return (
    <div>
      <TextField
        sx={{
          mb: 2,
          width: '100%',
        }}
        {...{ type, label, disabled, value, onChange }}
      />
    </div>
  );
}
