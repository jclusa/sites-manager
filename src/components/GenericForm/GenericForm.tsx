import { FC, Fragment } from 'react';
import { FormGroup } from '../FormGroup/FormGroup';
import { GenericFormField } from '../../types/genericFormField';
import { GenericError } from '../../types/error';
import { Error } from '../Error/Error';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

export interface GenericFormProps {
  fields: GenericFormField[];
  disabled: boolean;
  formObject: Record<string, string | null>;
  submitButtonText: string;
  error: GenericError;
  onChange: (name: string, value: string) => void;
  onSubmit: (ev: React.FormEvent) => void;
}

export const GenericForm: FC<GenericFormProps> = ({
  fields,
  disabled,
  formObject,
  submitButtonText,
  error,
  onChange,
  onSubmit,
}) => (
  <Fragment>
    <Error error={error} />

    <Box component="form" onSubmit={onSubmit}>
      {fields.map((field) => (
        <FormGroup
          key={field.name}
          disabled={disabled}
          type={field.type}
          label={field.label}
          value={formObject[field.name] || ''}
          onChange={onUpdateField(field.name, onChange)}
        />
      ))}
      <Button variant="outlined" type="submit">
        {submitButtonText}
      </Button>
    </Box>
  </Fragment>
);

function onUpdateField(
  name: string,
  onChange: GenericFormProps['onChange']
): (ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void {
  return ({ target: { value } }) => {
    onChange(name, value);
  };
}
