import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';
import HomeIcon from '@mui/icons-material/Home';
import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';

export function Header() {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" sx={{ flexGrow: 1 }}>
          Sites Manager
        </Typography>
        <Box sx={{ display: { xs: 'none', sm: 'revert' } }}>
          <Button component={Link} to="" color="inherit" startIcon={<HomeIcon />}>
            Home
          </Button>
          <Button component={Link} to="sites/create" color="inherit" startIcon={<AddIcon />}>
            New Site
          </Button>
        </Box>
        <Box sx={{ display: { xs: 'revert', sm: 'none' } }}>
          <IconButton component={Link} to="" color="inherit">
            <HomeIcon />
          </IconButton>
          <IconButton component={Link} to="sites/create" color="inherit">
            <AddIcon />
          </IconButton>
        </Box>
      </Toolbar>
    </AppBar>
  );
}
