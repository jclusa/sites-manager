import { Err, Ok } from '@hqoss/monads';
// eslint-disable-next-line
// @ts-ignore
import { act, fireEvent, render, screen } from '@testing-library/react';
import { createSite } from '../../../services/main';
import { store } from '../../../state/store';
import { initializeEditor } from '../../SiteEditor/SiteEditor.slice';
import { CreateSite } from './CreateSite';

jest.mock('../../../services/main.ts');

const mockedCreateSite = createSite as jest.Mock<ReturnType<typeof createSite>>;

beforeEach(() => {
  act(() => {
    store.dispatch(initializeEditor());
    render(<CreateSite />);
  });
});

it('Should update errors if save site fails', async () => {
  mockedCreateSite.mockResolvedValueOnce(Err({ error: 'path is required' }));
  await act(async () => {
    fireEvent.click(screen.getByText('Save Site'));
  });

  expect(screen.getByText('path is required')).toBeInTheDocument();
});

it('Should redirect to site if save is successful', async () => {
  mockedCreateSite.mockResolvedValueOnce(
    Ok({
      _id: 'testId',
      name: 'Antena 3',
      path: 'https://private.antena3.com/',
      publicPath: 'https://www.antena3.com/',
      key: 'antena3',
      description: 'Pagina web de antena 3',
      site: '1648316928647',
      createDate: '2022-03-26T17:48:48.602Z',
      __v: 0,
    })
  );
  await act(async () => {
    fireEvent.click(screen.getByText('Save Site'));
  });

  expect(location.hash).toMatch('#/sites/testId');
});
