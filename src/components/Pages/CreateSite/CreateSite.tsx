import { FormEvent, useEffect } from 'react';
import { createSite } from '../../../services/main';
import { store } from '../../../state/store';
import { SiteEditor } from '../../SiteEditor/SiteEditor';
import { initializeEditor, startSubmitting, updateError } from '../../SiteEditor/SiteEditor.slice';

export function CreateSite() {
  useEffect(() => {
    store.dispatch(initializeEditor());
  }, [null]);

  return <SiteEditor onSubmit={onSubmit} />;
}

async function onSubmit(ev: FormEvent) {
  ev.preventDefault();
  store.dispatch(startSubmitting());
  const result = await createSite(store.getState().editor.site);

  result.match({
    err: (error) => store.dispatch(updateError(error)),
    ok: ({ _id }) => {
      location.hash = `#/sites/${_id}`;
    },
  });
}
