import { Err, Ok } from '@hqoss/monads';
// eslint-disable-next-line
// @ts-ignore
import { act, fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import { getSite, updateSite } from '../../../services/main';
import { store } from '../../../state/store';
import { initializeEditor } from '../../SiteEditor/SiteEditor.slice';
import { EditSite } from './EditSite';

jest.mock('../../../services/main.ts');

const mockedGetSite = getSite as jest.Mock<ReturnType<typeof getSite>>;
const mockedUpdateSite = updateSite as jest.Mock<ReturnType<typeof updateSite>>;

const defaultSite = {
  _id: '1234',
  name: 'Antena 3',
  path: 'https://private.antena3.com/',
  publicPath: 'https://www.antena3.com/',
  key: 'antena3',
  description: 'Pagina web de antena 3',
  site: '1648316928647',
  createDate: '2022-03-26T17:48:48.602Z',
  __v: 0,
};

beforeEach(async () => {
  await act(async () => {
    store.dispatch(initializeEditor());
  });
});

async function renderWithPath(siteId: string) {
  await act(async () => {
    render(
      <MemoryRouter initialEntries={[`/${siteId}`]}>
        <Routes>
          <Route path="/:_id" element={<EditSite />} />
        </Routes>
      </MemoryRouter>
    );
  });
}

it('Should redirect to home if site load fails', async () => {
  location.hash = '#/sites/1234/edit';
  mockedGetSite.mockRejectedValueOnce({});

  await renderWithPath('1234');

  expect(location.hash).toMatch(/#\/$/);
});

it('Should load site', async () => {
  location.hash = '#/sites/1234/edit';
  mockedGetSite.mockResolvedValueOnce(defaultSite);
  await renderWithPath('1234');

  expect(store.getState().editor.loading).toBeFalsy();
  expect(store.getState().editor.site.name).toMatch(defaultSite.name);
});

it('Should update errors if save site fails', async () => {
  mockedGetSite.mockResolvedValueOnce(defaultSite);
  mockedUpdateSite.mockResolvedValueOnce(Err({ error: 'path is required' }));

  await renderWithPath('1234');
  await act(async () => {
    fireEvent.click(screen.getByText('Save Site'));
  });

  expect(screen.getByText('path is required')).toBeInTheDocument();
});

it('Should redirect to article if update is successful', async () => {
  mockedGetSite.mockResolvedValueOnce(defaultSite);
  mockedUpdateSite.mockResolvedValueOnce(Ok(defaultSite));

  await renderWithPath('1234');
  await act(async () => {
    fireEvent.click(screen.getByText('Save Site'));
  });

  expect(location.hash).toMatch(`#/sites/${defaultSite._id}`);
});
