import React, { Fragment, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { getSite, updateSite } from '../../../services/main';
import { store } from '../../../state/store';
import { useStore } from '../../../state/storeHooks';
import { SiteEditor } from '../../SiteEditor/SiteEditor';
import { initializeEditor, loadSite, startSubmitting, updateError } from '../../SiteEditor/SiteEditor.slice';

export interface Params {
  _id: string;
}

export function EditSite() {
  const { _id } = useParams<keyof Params>() as Params;
  const { loading } = useStore(({ editor }) => editor);

  useEffect(() => {
    _loadSite(_id);
  }, [_id]);

  return <Fragment>{!loading && <SiteEditor onSubmit={onSubmit(_id)} />}</Fragment>;
}

async function _loadSite(_id: string) {
  store.dispatch(initializeEditor());
  try {
    const { name, path, publicPath, key, description } = await getSite(_id);

    store.dispatch(loadSite({ name, path, publicPath, key, description }));
  } catch {
    location.hash = '#/';
  }
}

function onSubmit(_id: string): (ev: React.FormEvent) => void {
  return async (ev) => {
    ev.preventDefault();

    store.dispatch(startSubmitting());
    const result = await updateSite(_id, store.getState().editor.site);

    result.match({
      err: (error) => store.dispatch(updateError(error)),
      ok: ({ _id }) => {
        location.hash = `#/sites/${_id}`;
      },
    });
  };
}
