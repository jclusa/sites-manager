// eslint-disable-next-line
// @ts-ignore
import { act, fireEvent, render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { deleteSite, getSites } from '../../../services/main';
import { Home } from './Home';

jest.mock('../../../services/main');

const mockedGetSites = getSites as jest.Mock<ReturnType<typeof getSites>>;
const mockedDeleteSite = deleteSite as jest.Mock<ReturnType<typeof deleteSite>>;

const defaultSite = {
  _id: '1234',
  name: 'Antena 3',
  path: 'https://private.antena3.com/',
  publicPath: 'https://www.antena3.com/',
  key: 'antena3',
  description: 'Pagina web de antena 3',
  site: '1648316928647',
  createDate: '2022-03-26T17:48:48.602Z',
  __v: 0,
};

async function renderHome() {
  await act(async () => {
    await render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
  });
}

it('Should load sites', async () => {
  mockedGetSites.mockResolvedValueOnce([defaultSite]);

  await renderHome();

  expect(screen.getByText('Antena 3')).toBeInTheDocument();
  expect(screen.getByText('https://www.antena3.com/')).toBeInTheDocument();
});

it('Should delete site', async () => {
  mockedGetSites.mockResolvedValueOnce([{ ...defaultSite, name: 'This is a test site A' }]);

  await renderHome();

  mockedDeleteSite.mockResolvedValueOnce();
  mockedGetSites.mockResolvedValueOnce([{ ...defaultSite, name: 'This is a test site B after' }]);
  await act(async () => {
    fireEvent.click(screen.getByLabelText('Delete site 1'));
  });

  expect(screen.queryByText('This is a test site A')).not.toBeInTheDocument();
  expect(screen.getByText('This is a test site B after')).toBeInTheDocument();
  expect(mockedDeleteSite.mock.calls).toHaveLength(1);
  expect(mockedDeleteSite.mock.calls[0][0]).toMatch(defaultSite._id);
});
