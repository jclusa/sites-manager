import { getSites } from '../../../services/main';
import { useStoreWithInitializer } from '../../../state/storeHooks';
import { loadSites, startLoadingSites } from '../../SitesTable/SitesTable.slice';
import { store } from '../../../state/store';
import { SitesTable } from '../../SitesTable/SitesTable';

export function Home() {
  useStoreWithInitializer(() => {}, load);

  return (
    <div>
      <SitesTable />
    </div>
  );
}

async function load() {
  store.dispatch(startLoadingSites());

  const multipleSites = await getSites();
  store.dispatch(loadSites(multipleSites));
}
