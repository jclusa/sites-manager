import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { None, Option, Some } from '@hqoss/monads';
import { Site } from '../../../types/site';

export interface ShowSiteState {
  site: Option<Site>;
}

const initialState: ShowSiteState = {
  site: None,
};

const slice = createSlice({
  name: 'showSite',
  initialState,
  reducers: {
    initializeShowSite: () => initialState,
    loadSite: (state, { payload: site }: PayloadAction<Site>) => {
      state.site = Some(site);
    },
  },
});

export const { initializeShowSite, loadSite } = slice.actions;

export default slice.reducer;
