import { Ok } from '@hqoss/monads';
// eslint-disable-next-line
// @ts-ignore
import { act, render, screen } from '@testing-library/react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import { getSite } from '../../../services/main';

import { redirect } from '../../../utils/utils';
import { ShowSite } from './ShowSite';

jest.mock('../../../services/main.ts');

const mockedGetSite = getSite as jest.Mock<ReturnType<typeof getSite>>;

const defaultSite = {
  _id: '1234',
  name: 'Antena 3',
  path: 'https://private.antena3.com/',
  publicPath: 'https://www.antena3.com/',
  key: 'keyantena3',
  description: 'Pagina web de antena 3',
  site: '1648316928647',
  createDate: '2022-03-26T17:48:48.602Z',
  __v: 0,
};

async function renderWithPath(id: string) {
  await act(async () => {
    render(
      <MemoryRouter initialEntries={[`/${id}`]}>
        <Routes>
          <Route path="/:id" element={<ShowSite />} />
        </Routes>
      </MemoryRouter>
    );
  });
}

describe('In the show site page', () => {
  it('Should redirect to home if it fails to load site', async () => {
    redirect('sites/something');
    mockedGetSite.mockRejectedValueOnce({});
    await renderWithPath('124323');

    expect(location.hash === '#/').toBeTruthy();
  });

  it('Should render site', async () => {
    mockedGetSite.mockResolvedValueOnce(defaultSite);
    await renderWithPath('1234');

    expect(screen.getByText('1234')).toBeInTheDocument();
    expect(screen.getByText('Antena 3')).toBeInTheDocument();
    expect(screen.getByText('https://private.antena3.com/')).toBeInTheDocument();
    expect(screen.getByText('https://www.antena3.com/')).toBeInTheDocument();
    expect(screen.getByText('keyantena3')).toBeInTheDocument();
    expect(screen.getByText('Pagina web de antena 3')).toBeInTheDocument();
    expect(screen.getByText('1648316928647')).toBeInTheDocument();
    expect(screen.getByText('Mar 26, 2022')).toBeInTheDocument();
  });
});
