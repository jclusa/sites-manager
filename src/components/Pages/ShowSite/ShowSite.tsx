import Typography from '@mui/material/Typography';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { getSite } from '../../../services/main';
import { store } from '../../../state/store';
import { useStore } from '../../../state/storeHooks';
import { Site } from '../../../types/site';
import { formatDate, redirect } from '../../../utils/utils';
import { initializeShowSite, loadSite } from './ShowSite.slice';
import styles from './ShowSite.module.css';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';

export interface Params {
  _id: string;
}

export function ShowSite() {
  const { _id } = useParams<keyof Params>() as Params;

  const {
    showSite: { site },
  } = useStore(({ showSite }) => ({
    showSite,
  }));

  useEffect(() => {
    onLoad(_id);
  }, [_id]);

  return site.match({
    none: () => <div>Loading site...</div>,
    some: (site) => <SiteInfo site={site} />,
  });
}

async function onLoad(_id: string) {
  store.dispatch(initializeShowSite());

  try {
    const site = await getSite(_id);
    store.dispatch(loadSite(site));
  } catch (err) {
    redirect('');
  }
}

function SiteInfo({ site }: { site: Site }) {
  return (
    <Container maxWidth="sm" sx={{ mt: 4 }}>
      <Attribute name="Name" value={site.name} />
      <AttributeLink name="Path" value={site.path} />
      <AttributeLink name="Public Path" value={site.publicPath} />
      <Attribute name="Key" value={site.key} />
      <Attribute name="Description" value={site.description} />
      <Attribute name="ID" value={site._id} />
      <Attribute name="Site #" value={site.site} />
      <Attribute name="Create Date" value={formatDate(site.createDate)} />
    </Container>
  );
}

function Attribute({ name, value }: { name: string; value: string }) {
  return (
    <div className={styles.attribute}>
      <Typography variant="body1">
        <strong>{name}</strong>
      </Typography>
      <Typography variant="body1">{value}</Typography>
    </div>
  );
}

function AttributeLink({ name, value }: { name: string; value: string }) {
  return (
    <div className={styles.attribute}>
      <Typography variant="body1">
        <strong>{name}</strong>
      </Typography>
      <Link variant="body1" href={value} target="_blank" rel="noopener">
        {value}
      </Link>
    </div>
  );
}
