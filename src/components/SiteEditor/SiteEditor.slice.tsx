import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SiteForEditor } from '../../types/site';
import { GenericError } from '../../types/error';

export interface EditorState {
  site: SiteForEditor;
  submitting: boolean;
  error: GenericError;
  loading: boolean;
}

const initialState: EditorState = {
  site: { name: '', path: '', publicPath: '', key: '', description: '' },
  submitting: false,
  error: {},
  loading: true,
};

const slice = createSlice({
  name: 'editor',
  initialState,
  reducers: {
    initializeEditor: () => initialState,
    updateField: (
      state,
      { payload: { name, value } }: PayloadAction<{ name: keyof EditorState['site']; value: string }>
    ) => {
      state.site[name] = value;
    },
    updateError: (state, { payload: error }: PayloadAction<GenericError>) => {
      state.error = error;
      state.submitting = false;
    },
    startSubmitting: (state) => {
      state.submitting = true;
    },
    loadSite: (state, { payload: site }: PayloadAction<SiteForEditor>) => {
      state.site = site;
      state.loading = false;
    },
  },
});

export const { initializeEditor, updateField, startSubmitting, updateError, loadSite } = slice.actions;

export default slice.reducer;
