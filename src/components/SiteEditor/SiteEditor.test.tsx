// eslint-disable-next-line
// @ts-ignore
import { act, fireEvent, render, screen } from '@testing-library/react';
import { store } from '../../state/store';
import { SiteEditor } from './SiteEditor';
import { initializeEditor } from './SiteEditor.slice';

beforeEach(() => {
  act(() => {
    store.dispatch(initializeEditor());
    render(<SiteEditor onSubmit={(ev) => ev.preventDefault()} />);
  });
});

it('Should update site text fields', async () => {
  await act(async () => {
    fireEvent.change(screen.getByLabelText('Name'), { target: { value: 'testName' } });
  });
  await act(async () => {
    fireEvent.change(screen.getByLabelText('Path'), { target: { value: 'testPath' } });
  });
  await act(async () => {
    fireEvent.change(screen.getByLabelText('Public Path'), { target: { value: 'testPublicPath' } });
  });
  await act(async () => {
    fireEvent.change(screen.getByLabelText('Key'), { target: { value: 'testKey' } });
  });
  await act(async () => {
    fireEvent.change(screen.getByLabelText('Description'), { target: { value: 'testDescription' } });
  });

  expect(store.getState().editor.site.name).toMatch('testName');
  expect(store.getState().editor.site.path).toMatch('testPath');
  expect(store.getState().editor.site.publicPath).toMatch('testPublicPath');
  expect(store.getState().editor.site.key).toMatch('testKey');
  expect(store.getState().editor.site.description).toMatch('testDescription');
});
