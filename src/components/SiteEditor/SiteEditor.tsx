import { store } from '../../state/store';
import { useStore } from '../../state/storeHooks';
import { buildGenericFormField } from '../../types/genericFormField';
import { GenericForm } from '../GenericForm/GenericForm';
import { EditorState, updateField } from './SiteEditor.slice';
import Container from '@mui/material/Container';
import styles from './SiteEditor.module.css';

export function SiteEditor({ onSubmit }: { onSubmit: (ev: React.FormEvent) => void }) {
  const { site, error, submitting } = useStore(({ editor }) => editor);

  return (
    <Container maxWidth="sm" className={styles.container}>
      <GenericForm
        formObject={{ ...site } as unknown as Record<string, string | null>}
        disabled={submitting}
        error={error}
        onChange={onUpdateField}
        onSubmit={onSubmit}
        submitButtonText="Save Site"
        fields={[
          buildGenericFormField({ name: 'name', label: 'Name' }),
          buildGenericFormField({ name: 'path', label: 'Path' }),
          buildGenericFormField({ name: 'publicPath', label: 'Public Path' }),
          buildGenericFormField({ name: 'key', label: 'Key' }),
          buildGenericFormField({ name: 'description', label: 'Description' }),
        ]}
      />
    </Container>
  );
}

function onUpdateField(name: string, value: string) {
  store.dispatch(updateField({ name: name as keyof EditorState['site'], value }));
}
