import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import { Site } from '../../types/site';
import Stack from '@mui/material/Stack';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Link as RouterLink } from 'react-router-dom';
import { store } from '../../state/store';
import { loadSites } from '../SitesTable/SitesTable.slice';
import { deleteSite, getSites } from '../../services/main';
import { formatDate } from '../../utils/utils';
import Link from '@mui/material/Link';

export function SiteRow({ site, index }: { site: Site; index: number }) {
  return (
    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
      <TableCell component="th" scope="row">
        {site.name}
      </TableCell>
      <TableCell sx={{ display: { xs: 'none', sm: 'revert' } }}>
        <Link href={site.publicPath} target="_blank" rel="noopener">
          {site.publicPath}
        </Link>
      </TableCell>
      <TableCell sx={{ display: { xs: 'none', md: 'revert' } }}>{formatDate(site.createDate)}</TableCell>
      <TableCell>
        <Stack direction="row" spacing={1}>
          <RouterLink to={`/sites/${site._id}`}>
            <IconButton>
              <VisibilityIcon />
            </IconButton>
          </RouterLink>
          <RouterLink to={`/sites/${site._id}/edit`}>
            <IconButton>
              <EditIcon />
            </IconButton>
          </RouterLink>
          <IconButton aria-label={`Delete site ${index + 1}`} onClick={() => onDeleteSite(site._id)}>
            <DeleteIcon />
          </IconButton>
        </Stack>
      </TableCell>
    </TableRow>
  );
}

async function onDeleteSite(_id: string) {
  await deleteSite(_id);
  store.dispatch(loadSites(await getSites()));
}
