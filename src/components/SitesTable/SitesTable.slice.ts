import { Some, Option, None } from '@hqoss/monads';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Site } from '../../types/site';

export interface SitesTableState {
  sites: Option<Site[]>;
}

const initialState: SitesTableState = {
  sites: None,
};

const slice = createSlice({
  name: 'sitesTable',
  initialState,
  reducers: {
    startLoadingSites: () => initialState,
    loadSites: (state, { payload: sites }: PayloadAction<Site[]>) => {
      state.sites = Some(sites);
    },
  },
});

export const { startLoadingSites, loadSites } = slice.actions;

export default slice.reducer;
