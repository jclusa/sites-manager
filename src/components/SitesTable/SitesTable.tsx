import { useStore } from '../../state/storeHooks';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { SiteRow } from '../SiteRow/SiteRow';

export function SitesTable() {
  const { sites } = useStore(({ sitesTable }) => sitesTable);

  return sites.match({
    none: () => <div>Loading sites...</div>,
    some: (sites) => (
      <TableContainer component={Paper} sx={{ m: 4, width: 'auto' }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell sx={{ display: { xs: 'none', sm: 'revert' } }}>Public Path</TableCell>
              <TableCell sx={{ display: { xs: 'none', md: 'revert' } }}>Create Date</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sites.map((site, index) => (
              <SiteRow key={site._id} site={site} index={index} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    ),
  });
}
