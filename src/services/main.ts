import { Err, Ok, Result } from '@hqoss/monads';
import axios from 'axios';
import settings from '../config/settings';
import { Site, multipleSitesDecoder, siteDecoder, SiteForEditor } from '../types/site';
import { GenericError, genericErrorDecoder } from '../types/error';

axios.defaults.baseURL = settings.baseApiUrl;

export async function getSites(): Promise<Site[]> {
  return multipleSitesDecoder.verify((await axios.get('sites')).data);
}

export async function deleteSite(_id: string): Promise<void> {
  await axios.delete(`site/${_id}`);
}

export async function getSite(_id: string): Promise<Site> {
  const { data } = await axios.get(`site/${_id}`);
  return siteDecoder.verify(data);
}

export async function updateSite(_id: string, site: SiteForEditor): Promise<Result<Site, GenericError>> {
  try {
    const { data } = await axios.put(`site/${_id}`, site);

    return Ok(siteDecoder.verify(data));
  } catch ({ response: { data } }) {
    return Err(genericErrorDecoder.verify(data));
  }
}

export async function createSite(site: SiteForEditor): Promise<Result<Site, GenericError>> {
  try {
    const { data } = await axios.post('site', site);

    return Ok(siteDecoder.verify(data));
  } catch ({ response: { data } }) {
    return Err(genericErrorDecoder.verify(data));
  }
}
