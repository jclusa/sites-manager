import { Action, configureStore } from '@reduxjs/toolkit';
import sitesTable from '../components/SitesTable/SitesTable.slice';
import editor from '../components/SiteEditor/SiteEditor.slice';
import showSite from '../components/Pages/ShowSite/ShowSite.slice';

const middlewareConfiguration = { serializableCheck: false };

export const store = configureStore({
  reducer: { sitesTable, editor, showSite },
  devTools: {
    name: 'SitesManager',
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware(middlewareConfiguration),
});
export type State = ReturnType<typeof store.getState>;

export function dispatchOnCall(action: Action) {
  return () => store.dispatch(action);
}
