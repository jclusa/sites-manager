// eslint-disable-next-line
// @ts-ignore
import { dict, string } from 'decoders';

export type GenericError = Record<string, string>;

export const genericErrorDecoder = dict(string);
