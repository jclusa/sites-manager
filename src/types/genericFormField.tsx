export interface GenericFormField {
  name: string;
  type: string;
  label: string;
  fieldType: 'input';
}

export function buildGenericFormField(data: Partial<GenericFormField> & { name: string }): GenericFormField {
  return {
    type: 'text',
    label: '',
    fieldType: 'input',
    ...data,
  };
}
