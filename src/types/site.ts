// eslint-disable-next-line
// @ts-ignore
import { array, Decoder, number, object, string } from 'decoders';

export interface Site {
  _id: string;
  name: string;
  path: string;
  publicPath: string;
  key: string;
  description: string;
  site: string;
  createDate: string;
  __v: number;
}

export const siteDecoder: Decoder<Site> = object({
  _id: string,
  name: string,
  path: string,
  publicPath: string,
  key: string,
  description: string,
  site: string,
  createDate: string,
  __v: number,
});

export const multipleSitesDecoder: Decoder<Site[]> = array(siteDecoder);

export interface SiteForEditor {
  name: string;
  path: string;
  publicPath: string;
  key: string;
  description: string;
}
