import { format } from 'date-fns';

export function redirect(path: string) {
  location.hash = `#/${path}`;
}

export function formatDate(date: string) {
  return format(new Date(date), 'PP');
}
